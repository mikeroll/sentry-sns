# sentry-sns
A sentry plugin which allows sending project alerts to Amazon SNS, to be later routed to email, SMS and other types of subscribers.

The repository includes:
- `sentry-bundle` - a dockerfile for building a bundled `sentry` + `sentry-sns` image 
- `sentry-bundle/plugins/sentry-sns` - the plugin itself
- `terraform/sns_sms_subscription` - a terraform module for creating an SNS topic + adding subscriptions
- `nope` - an example app that always fails and sends events to sentry
- `demo` - an example configuration to see it all in action
