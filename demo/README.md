# sentry-sns demo

### Create SNS topic and subscriptions
Provide terraform variables whichever way suits you, then run terraform as usual.

```sh
$ cd terraform/

$ > terraform.tfvars <<EOF
aws_access_key = "key_id"
aws_secret_key = "secret"
phone_numbers = ["+3752500000000", "+3752900000000"]
EOF

$ terraform init
$ terraform plan
$ terraform apply

$ cd ..
```

### Configure & run sentry
The provided `docker-compose.yml` is set to run the build and run sentry from `../sentry-bundle` so that the plugin is included automatically. 

Create a secret key and save it into `.env`:
```sh
$ echo SENTRY_SECRET_KEY="$(docker-compose run sentry generate-secret-key)" > .env
```

Set AWS credentials for the plugin:
```sh
$ echo SENTRY_SNS_ACCESS_KEY="access_key" >> .env
$ echo SENTRY_SNS_SECRET_KEY="secret_key" >> .env
```

Run sentry:
```sh
$ docker-compose run sentry-upgrade
$ docker-compose up --build sentry sentry-cron sentry-worker
```

### Create and configure project
Visit sentry at `http://localhost:9000`, create a project for the `nope` app and copy its DSN. Make sure the host part of the DSN is accessible from inside the app container - e.g. use `http://<public_key>@sentry:9000/<proj_id>`
```sh
$ echo "NOPE_SENTRY_DSN=<dsn>" >> .env
```

Configure the SNS plugin in `Project Settings > Alerts > Settings` and enable route alerts to the plugin in `Project Settings > Alerts > Rules`.

### Run the sample app
Start the app container, curl it any request (spoiler - it will fail) and wait for an sms.
```sh
$ docker-compose up -d nope
$ curl -XGET localhost:9009/rich_fast
