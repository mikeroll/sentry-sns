provider "aws" {
  version = "~> 2.4"

  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

module "nope_sms_alerts" {
  source = "../../terraform/sns_sms_subscription"

  topic_name         = "nope-alerts"
  topic_display_name = "Nope"

  phone_numbers = ["${var.phone_numbers}"]
}
