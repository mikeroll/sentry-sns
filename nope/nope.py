import os
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

import sentry_sdk


class NopeException(Exception):
    def __init__(self, command, path):
        self.command = command
        self.path = path
        return super(NopeException, self).__init__(
            '{0} {1} - NOPE. Not gonna happen.'.format(command, path)
        )


class NopeHandler(BaseHTTPRequestHandler):
    def respond(self):
        try:
            raise NopeException(self.command, self.path)
        except Exception as e:
            sentry_sdk.capture_exception(e)
            self.send_error(500, str(e))

    def __getattr__(self, attr):
        if attr.startswith('do_'):
            return self.respond

        return super(BaseHTTPRequestHandler, self).__getattr__(attr)


def run(host, port):
    server = HTTPServer((host, port), NopeHandler)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass

    server.server_close()


def before_send(event, hint):
    if 'exc_info' not in hint:
        return event

    exception = hint['exc_info'][1]
    if isinstance(exception, NopeException):
        event['fingerprint'] = [
            '{{ default }}',
            exception.command,
            exception.path,
        ]

    return event


sentry_sdk.init(os.environ['NOPE_SENTRY_DSN'], before_send=before_send)
run(
    host=os.getenv('NOPE_HOST', '0.0.0.0'),
    port=int(os.getenv('NOPE_PORT', '9000')),
)
