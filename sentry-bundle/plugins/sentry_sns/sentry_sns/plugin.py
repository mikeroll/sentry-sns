import os

import boto3
from sentry import ratelimits
from sentry.plugins.bases.notify import NotifyPlugin


class SNSNotificationPlugin(NotifyPlugin):
    title = 'Amazon SNS'
    slug = 'sentry-sns'
    description = 'Send project alerts to an SNS topic'

    conf_key = 'sentry_sns'
    conf_title = title

    def is_configured(self, project, **kwargs):
        return all((bool(val) for val in (
            self.get_option('sns_topic', project),
            self.get_option('region', project),
            os.getenv('SENTRY_SNS_ACCESS_KEY'),
            os.getenv('SENTRY_SNS_SECRET_KEY'),
        )))

    def get_available_regions(self):
        return boto3.Session().get_available_regions('sns')

    def get_config(self, project, **kwargs):
        return [
            {
                'name': 'sns_topic',
                'label': 'SNS Topic ARN',
                'type': 'string',
            },
            {
                'name': 'rate_limit',
                'label': 'Notification rate limit (messages per minute)',
                'type': 'number',
                'default': 10,
            },
            {
                'name': 'region',
                'label': 'Region',
                'type': 'select',
                'choices': tuple((r, r) for r in self.get_available_regions()),
            },
        ]

    def get_sns_client(self, project):
        access_key = os.environ['SENTRY_SNS_ACCESS_KEY']
        secret_key = os.environ['SENTRY_SNS_SECRET_KEY']
        region = self.get_option('region', project)

        client = boto3.client(
            service_name='sns',
            aws_access_key_id=access_key,
            aws_secret_access_key=secret_key,
            region_name=region,
        )
        client.set_sms_attributes(attributes={
            'DefaultSMSType': 'Transactional'
        })
        return client

    def notify_users(self, group, event, fail_silently=False):
        sns_client = self.get_sns_client(project=event.project)
        topic_arn = self.get_option('sns_topic', event.project)

        sns_client.publish(
            TopicArn=topic_arn,
            Message=event.message,
        )

    def __is_rate_limited(self, group, event):
        rate_limit = self.get_option('rate_limit', event.project)
        return ratelimits.is_limited(
            project=group.project,
            key=self.get_conf_key(),
            limit=rate_limit,
        )
