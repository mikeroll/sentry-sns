resource "aws_sns_topic" "topic" {
  name         = "${var.topic_name}"
  display_name = "${var.topic_display_name}"
}

resource "aws_sns_topic_subscription" "sms_subscription" {
  count = "${length(var.phone_numbers)}"

  topic_arn = "${aws_sns_topic.topic.arn}"
  protocol  = "sms"
  endpoint  = "${element(var.phone_numbers, count.index)}"
}
