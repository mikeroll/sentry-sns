output "topic_arn" {
  value = "${aws_sns_topic.topic.arn}"
}

output "subscription_arns" {
  value = ["${aws_sns_topic_subscription.sms_subscription.*.arn}"]
}
