variable "topic_name" {
  type = "string"
}

variable "topic_display_name" {
  type = "string"
}

variable "phone_numbers" {
  type    = "list"
  default = []
}
